var mysql = require('mysql');

var sql_requests = require("./sql-requests");

function setDatabaseConnection(){
	//console.log(process.env.HOST + " " + process.env.USERDB + " " + process.env.PASSWORDDB);
	
    DBController.prototype.db = mysql.createConnection({
		host: process.env.HOST,
		user: process.env.USERDB,
		password:process.env.PASSWORDDB,
		database : process.env.DB,
		connectTimeout: 60000,  
		supportBigNumbers : true,
		queueLimit : 0,  
		connectionLimit : 0,
		port:3306
    });

    DBController.prototype.db.on('error', (error) => {
        if(error.code === 'PROTOCOL_CONNECTION_LOST') {        
            //Reconnect              
            setDatabaseConnection();
        }
        if(error.code === "ER_NOT_SUPPORTED_AUTH_MODE") {
            console.log("Please specify database information"); 
        }
        else {                     
            console.log(error);                  
            setDatabaseConnection();
        }
    });

    DBController.prototype.db.connect((error) => {
        if(error) {
            console.log(error);
            setTimeout(setDatabaseConnection, 5000); 
        }
         else{
              //console.log("Database connection established");
         }
    });
}

var DBController = function() {
   setDatabaseConnection();
}

DBController.prototype.addDonation =  function(id, amount, description, accountNumber, callback){    
    //Preparing data for blockchain 
    var timeStamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
    DBController.prototype.db.query(sql_requests.ADD_DONATION, 
        [id, amount, timeStamp, description, accountNumber], function(error, result, fields){ 
             if(error) {
                console.log(error);
                callback(error, 0)
            }
            else {
                callback(null, 1)
            }
    });
}


DBController.prototype.getAccountDonatation =   function (accountNumber, callback){
    DBController.prototype.db.query(sql_requests.GET_ACCOUNT_DONATATION, [accountNumber],
            function(error, result, fields){ 
            if(error){ 
                callback(error, null)
            } else {
                callback(null, result[0]);     
            }   
    });
}

DBController.prototype.getDonations =  function(donateId, callBack){
    DBController.prototype.db.query(sql_requests.GET_DONATION_INFO, [donateId],
            function(error, result, fields){
                if(error){ 
                    console.log(error + "");
                    callBack(error, null); 
                } else {
                    callBack(null, result[0]);
            }
    });
}


DBController.prototype.registerTransacation =  function(donateId, amount, purpose, accountNumber, callBack){    
    DBController.prototype.db.query(sql_requests.GET_TRANSACATION_ID, function(er, re, fe){
        var timeStamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
        var transactionId = re[0]['GET_NEW_TRANSACATION_ID()'];

        DBController.prototype.db.query(sql_requests.ADD_TRANSACTION,  
            [transactionId,   donateId, amount, timeStamp, purpose, accountNumber],  function(error, result, fields){
            if(error) {
                // console.log(error);
                callBack(error, 0)
            }
            else {
                callBack(null, 1)
            }   
        });
    });
}
  
module.exports = DBController; 