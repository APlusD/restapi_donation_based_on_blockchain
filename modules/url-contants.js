module.exports = Object.freeze({
    MAIN_ADDRESS: '/', 
    REGISTAR_DONATE: '/registardonate', 
    GET_CHAIN: '/getdonatechain', 
    REGISTER_TRANSACATION: '/registertransacation',
    GET_ACCOUNT_DONATATION: '/getaccountdonations'
}); 