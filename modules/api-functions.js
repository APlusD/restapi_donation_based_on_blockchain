var createKeccakHash = require('keccak'); 

var DBController = require("./db-controller"); 
var dbController = new DBController()

 
var RESTAPIFunction = function() {
    //Use blockchain controller
}


RESTAPIFunction.prototype.main = function (req, res){
    res.send("Donatation chain module");
}              

RESTAPIFunction.prototype.addDonation =  function(req, res){
    var timeStamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
    var id = createKeccakHash('keccak256').update(req.query.amount.toString() + timeStamp.toString()).digest('hex').substring(0, 6);
    
    
    dbController.addDonation(id, req.query.amount, req.query.description, req.query.accountNumber, function (error, result){
        var output = {}; 
        output.key = id;
        output.result = result; 
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(output)) 
        //TODO Insert data to blockchain
    });
}

RESTAPIFunction.prototype.getDonations =  function (req, res){
    dbController.getDonations(req.query.donateId, function(error, queryResult){
        var output = {}; 
        if(error){
             output.result = 0; 
        }
        else {
            output.result = 1; 
            output.output = queryResult; 
        }
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(output)) 
        //TODO Verification data from blockchain
    });
}

RESTAPIFunction.prototype.getAccountDonatation =  function (req, res){
    dbController.getAccountDonatation(req.query.accountNumber, function(error, queryResult){
        var output = {}; 
        if(error){
             output.result = 0; 
        }
        else {
            output.result = 1; 
            output.output = queryResult; 
        }
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(output)) 
        //TODO Verification data from blockchain
    });
}    

RESTAPIFunction.prototype.registerTransacation = function (req, res){
    var output = {}; 
    dbController.registerTransacation(req.query.donateId, req.query.amount, req.query.purpose, req.query.accountNumber, 
            function(error, result){
        if(error){
            output.result = 0;
        }
        else{
            output.result = result; 
        }
        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(output)); 
        //TODO Insert data to blockchain 
    });
}
module.exports = RESTAPIFunction; 
