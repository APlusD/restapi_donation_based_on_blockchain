// Lowest level of the project is business logic that is implemented in the database(functions/procedures)  

var DBController = require("../../modules/db-controller"); 
var dbController = new DBController()

var assert = require('assert');
var mysql = require('mysql');

describe('DB controller functions:', ()=> {
    //Test data 
    var donationId = Math.random().toString(36).substring(7); //Should be new each time for a new donation 
    var count = Math.random() * (1000 - 100) + 100; //Random
    var accountNumber = "10000000000000000002" //For test account number
    
    //Check connection to database
    before('Connect to database', (done) => {
        dbController.db =  mysql.createConnection({
			host: process.env.HOST,
			user: process.env.USERDB,
			password:process.env.PASSWORDDB,
			database : process.env.DB,
			connectTimeout: 60000,  
			supportBigNumbers : true,
			queueLimit : 0,  
			connectionLimit : 0,
			port:3306
        });
        dbController.db.connect(done); 
    });    
     
    //Create a new donation on database 
    it("Add donation", (done)=>{
        dbController.addDonation(donationId, count, "test", accountNumber, (error, result)=>{
            assert.equal(result, 1);
            done(); 
        }); 
    }); 

    //Register a transaction of funds
    it("Register transaction - 1", (done)=> {
        dbController.registerTransacation(donationId, count - 5, "test", accountNumber, (error, result)=>{
            assert.equal(result, 1);
            done(); 
        });
    });

    it("Register transaction - 2", (done)=> {
        dbController.registerTransacation(donationId, count * count, "test", accountNumber, (error, result)=>{  
            assert.strictEqual(result, 0); //Try to send more funds that  the bank account contains (result should 0, with error not null_
            done(); 
        });
    });


    it("Get transactions chain of donation - 1", (done)=>{
        dbController.getDonations("3c99e3", (error, result)=>{ //Custom donation with chain of transactions 
            assert.equal(result.length, 5); //5 transactions should be in this donation
            done();
        });
    });

    it("Get transactions chain of donation - 2", (done)=>{
        dbController.getDonations("1223213", (error, result)=>{ //Random ID, not exception but empty result
            assert.equal(result, 0)
            done();
        });
    });


    it("Check account funds", (done)=>{
        dbController.getAccountDonatation("10000000000000000003", (error, result)=> {
            assert.equal(result, 0); 
            done(); 
        });
    });
	
	after(()=>{
		dbController.db.end(); //Close the connection 
    });
});

