//Higher level functions testing
//Checking REST API work and also how modules works together 

var app = require("../../index")
var url_addresses = require("../../modules/url-contants"); 

var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = chai.expect;
var assert = require('assert');

chai.use(chaiHttp);

describe('REST API:', ()=> {

    before((done)=>{
        setTimeout(done, 3000); //Time to starting server
    })
    
    //Test data 
    var count = Math.random() * (1000 - 100) + 100; //Random
    var accountNumber = "10000000000000000002" //Test account number
    var donateId; 

    it(url_addresses.REGISTAR_DONATE, (done)=>{
        chai.request(app)
        .get(url_addresses.REGISTAR_DONATE)
        .query({amount:count, description:"test", accountNumber:accountNumber})
        .end((err, res)=>{
            expect(res).to.have.status(200);
            expect(res).to.be.json;
            assert.equal(res.body["result"], 1);  //Good resposene data inserted to db
            donateId = res.body["key"];
            done(); 
        });
    });

    it(url_addresses.REGISTER_TRANSACATION + " - 1", (done)=>{
        chai.request(app)
        .get(url_addresses.REGISTER_TRANSACATION)
        .query({donateId:donateId, amount:100, description:"test", accountNumber:accountNumber})
        .end((err, res)=>{
            expect(res).to.have.status(200);
            expect(res).to.be.json;
            assert.equal(res.body["result"], 1);  //Good resposene data inserted to db
            done(); 
        });
    });

    it(url_addresses.REGISTER_TRANSACATION + " - 2", (done)=>{
        chai.request(app)
        .get(url_addresses.REGISTER_TRANSACATION)
        .query({donateId:donateId, amount:2000, description:"test", accountNumber:accountNumber})
        .end((err, res)=>{
            expect(res).to.have.status(200);
            expect(res).to.be.json;
            assert.equal(res.body["result"], 0);  //Good resposene, data not inserted to db
            done(); 
        });
    });

    it(url_addresses.GET_CHAIN + " - 1", (done)=>{
        chai.request(app)
        .get(url_addresses.GET_CHAIN)
        .query({donateId:"3c99e3"})
        .end((err, res)=>{
            expect(res).to.have.status(200);
            expect(res).to.be.json;
            assert.equal(res.body["result"], 1);  //Good response
            assert.equal(res.body["output"].length, 5); //Should be 5 elements for this donateid in db
            done(); 
        });
    });

    it(url_addresses.GET_CHAIN + " - 2", (done)=>{
        chai.request(app)
        .get(url_addresses.GET_CHAIN)
        .query({donateId:"111111"})
        .end((err, res)=>{
            expect(res).to.have.status(200);
            expect(res).to.be.json;
            assert.equal(res.body["result"], 1);  //Good response
            assert.equal(res.body["output"].length, 0); //Random id, not any transactions 
            done(); 
        });
    });
	
	after(()=>{
		return true;
    });

    
});

