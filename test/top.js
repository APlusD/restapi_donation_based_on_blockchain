describe("Top", ()=>{

    //Starting all tests
    describe("Unit tests", ()=>{
        require('./unit/DBTests.spec');
    });
    
    describe("Functional tests", ()=>{
       require("./functional/RESTAPITests.spec");
    });

    after(()=>{
        console.log("Tests passed");
		return true;
    });
});

