var express = require('express');
var cors = require('cors');
var app = express();

var RESTAPIFunction  = require('./modules/api-functions');
var apifunction = new RESTAPIFunction();
 
var helpfunction = require('./modules/helpfunction'); 
var url_addresses = require("./modules/url-contants"); 

app.use(cors());

app.use(helpfunction.setOptions);

// app.use(helpfunction.customLogger);

app.get(url_addresses.MAIN_ADDRESS, apifunction.main);

app.get(url_addresses.REGISTAR_DONATE, apifunction.addDonation); 

app.get(url_addresses.GET_CHAIN, apifunction.getDonations); 

app.get(url_addresses.REGISTER_TRANSACATION, apifunction.registerTransacation); 

app.get(url_addresses.GET_ACCOUNT_DONATATION, apifunction.getAccountDonatation);

var server = app.listen(process.env.PORT || 3999, function () {
    console.log("Server start working"); 
}); 


module.exports = server; //for tests
