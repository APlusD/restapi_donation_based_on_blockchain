# Donatation on blockchain

Dontation based on blockchain REST API. 
API use for registering transacations. 

[![Build Status](https://travis-ci.org/AplusD/restapi_donation_based_on_blockchain.svg?branch=master)](https://travis-ci.org/AplusD/restapi_donation_based_on_blockchain)

# Getting Started 

### REGISTER DONATE

#### Request
`GET /registardonate`
    Args: amount, description, accountNumber

#### Response
    {"key":"928db0", "result": 1} // RESULT: 1 -  success, 0 - failed


### GET CHAIN

#### Request
`GET /getdonatechain`
    Args: donateId

#### Response
    {"result": 1 , "output": [{"balance":0,"timeStamp":"2018-11-06T23:59:07.000Z","description":"prosto tak","stepId":3,"amount":123,"transitionTime":"2018-10-24T08:20:01.000Z","purpose":"Postuplenie na schet fonda","accountNumber":"10000000000000000003","taxId":"0000000003","organizationName":"Fond \"Kung-Fu Panda"},{"balance":0,"timeStamp":"2018-11-06T23:59:07.000Z","description":"prosto tak","stepId":1348,"amount":119,"transitionTime":"2018-11-06T22:40:08.000Z","purpose":"_","accountNumber":"5442110768","taxId":"2721055384","organizationName":"KGBU Detskiy Dom 1"},{"balance":0,"timeStamp":"2018-11-06T23:59:07.000Z","description":"prosto tak","stepId":1354,"amount":4,"transitionTime":"2018-11-06T22:55:34.000Z","purpose":"_","accountNumber":"15457164714","taxId":"3905010466","organizationName":"Detskaya Oblastnaya Bolnitca"}]} // RESULT: 1  -  success, 0 - failed, 2 - data not equal from blockchain


### GET ACCOUNT DONATATIONS

#### Request
`GET /getaccountdonations`
    Args: accountNumber

#### Response
    {"result":1,"output":[{"balance":10000,"donationId":"380dfa","timeStamp":"2018-11-03T21:51:55.000Z"}]} // RESULT: 1 -  success, 0 - failed,  2 - data not equal from blockchain

### REGISTER TRANSACTION

#### Request
`GET /registertransacation`
    Args: donateId, amount, purpose, accountNumber

#### Response
    {"result": 1 } // RESULT: 1 -  success, 0 - failed,  2 - data not equal from blockchain
 
 
## Testing the project  

Mocha.js is used for implementing tests. Chai.js is used for implementing requests for API.

#### Running tests: 
1. Clone the project from repository and open the directory.
```
git clone https://github.com/AplusD/restapi-testing-devops
cd restapi-testing-devops
```
2. Install all dependencies of the project.
```
npm install
```
3. Run the tests
```
npm test
```

## Authors

* **Dzhonov Azamat** - [Aplusd](https://github.com/aplusd)